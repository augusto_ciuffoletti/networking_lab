# Networking Laboratory HOWTO

**Preparation**

1. Install VirtualBox
1. Install Vagrant (Version > 1.6)
1. Install git

**Start your network**
```
git clone git@bitbucket.org:augusto_ciuffoletti/networking_lab.git
cd networking_lab
vagrant up
```
Wait while the disk image is downloaded and the virtual machines are booted.

**Login into your Virtual Machines**
```
vagrant ssh pc0
```
```
vagrant ssh pc1
```
**Stop a Virtual Machine**
```
vagrant halt pc0
```
**Restart Virtual Machines**
```
vagrant up
```
Disk content is preserved across Stop/Restart.